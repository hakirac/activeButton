## Active Button 
Please consider index.html
It shows active button or clicked button on the links. It is isomorphic library. 

### Demo Url
<div>
<a href="https://codepen.io/hakirac/project/editor/ZyMYYe#0"> Demo Url - Codepen </a>
</div>

#### Include style.css file between head tag
```html
<link rel="stylesheet" type="text/css" href="style.css">
```
#### Include activeButton.js file between head tag
```html
<script src="activeButton.js"></script>
```

#### Use the following form between body tag
```html
<div id="kategoriler"> 
    <a href="/bidb/dashboard"> Tümü </a>
    <a href="/bidb/dashboard/devameden" > Devam Edenler </a>
    <a href="/bidb/dashboard/gecti" > Geçti </a>
    <a href="/bidb/dashboard/donduruldu" > Donduruldu </a>
    <a href="/bidb/dashboard/iptal" > İptal </a>
    <a href="/bidb/dashboard/tamamlananlar"> Tamamlananlar </a>
</div>
```